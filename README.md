## qssi-user 13 TP1A.220905.001 1687181189345 release-keys
- Manufacturer: qualcomm
- Platform: holi
- Codename: holi
- Brand: qti
- Flavor: qssi-user
- Release Version: 13
- Kernel Version: 5.4.210
- Id: TP1A.220905.001
- Incremental: 1687181189345
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: zh-CN
- Screen Density: undefined
- Fingerprint: qti/holi/holi:12/RKQ1.211119.001/1687182918217:user/release-keys
- OTA version: 
- Branch: qssi-user-13-TP1A.220905.001-1687181189345-release-keys
- Repo: qti_holi_dump_892
